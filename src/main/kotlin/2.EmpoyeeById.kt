import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()
    val map = mutableMapOf<String, Employee>()
    for (i in 1..n) {
        val employee = Employee(scanner.next(),scanner.next(),scanner.next(),scanner.next())
        map.put(employee.dni, employee)
    }

    do {
        val dni = scanner.next()
        println(map[dni])
    } while(dni!="END")

}

data class Employee(val dni:String, val nom:String, val cognoms:String, val adreça:String)