import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val registres = scanner.nextInt()
    val map = mutableMapOf<Int,String>()
    for (i in 1..registres) {
        val km = scanner.nextInt()
        val cartell = scanner.next()
        map.put(km,cartell)
    }
    val consultes = scanner.nextInt()
    for (i in 1..consultes) {
        val km = scanner.nextInt()
        if (map.contains(km)) println(map[km])
        else println("No hi ha cartell")
    }

}