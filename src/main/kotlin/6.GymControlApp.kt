import java.util.*

fun main() {
    val control = GymControlManualReader()
    val idList = mutableListOf<String>()
    for (i in 1..8) {
        val id = control.nextId()
        if (id !in idList) {
            println("Entrada")
            idList.add(id)
        }
        else {
            println("Sortida")
            idList.remove(id)
        }
    }
}

interface GymControlReader {
    fun nextId() : String
}

class GymControlManualReader(val scanner: Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId() = scanner.next()

}
