import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val map = mutableMapOf<String, Int>()
    do {
        val student = scanner.next()
        if (map.contains(student)) map[student] = map[student]!! + 1
        else map.put(student,1)
    } while (student!="END")
    println(map)
}